package com.company.Interface;

import java.util.List;

public interface ReadFileIntf {
    List<String> readFile(String fileName);
}

package com.company.UtilClass;

import com.company.Interface.ReadFileIntf;
import com.company.Interface.ReiterationWordCountIntf;
import com.company.ReadFile;

import java.util.HashMap;
import java.util.Map;

public class ReiterationWordCount implements ReiterationWordCountIntf {
    private Map<String, Long> textMap = new HashMap<>();
    private ReadFileIntf readFileObj = new ReadFile();


    @Override
    public int reiterationWordCount(String fileName) {
        int count = 0;

        for (String str : readFileObj.readFile(fileName)) {
            if (textMap.get(str) == null) {
                textMap.put(str, (long) 1);
            } else {
                count++;
            }

        }
        return count;

    }
}

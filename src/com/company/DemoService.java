package com.company;

import com.company.Interface.DemoServiceIntf;
import com.company.Interface.ReiterationWordCountIntf;
import com.company.Interface.SortTextIntf;
import com.company.Interface.UniqueWordsCountIntf;
import com.company.UtilClass.ReiterationWordCount;
import com.company.UtilClass.SortText;
import com.company.UtilClass.UniqueWordsCount;

public class DemoService implements DemoServiceIntf {

    @Override
    public void demoService() {

        //Подсчет количества повторяющихся слов
        ReiterationWordCountIntf reiterationCount = new ReiterationWordCount();
        System.out.println(reiterationCount.reiterationWordCount("text.txt"));

        //Подсчет уникальных слов
        UniqueWordsCountIntf uniqueCount = new UniqueWordsCount();
        System.out.println(uniqueCount.uniqueWordsCount("text.txt"));

        //Сортировка по длине
        SortTextIntf sortTextObj = new SortText();
        System.out.println(sortTextObj.sortText("text.txt"));




    }


}

package com.company.Interface;

import java.util.List;

public interface SortTextIntf {
    List<String> sortText(String fileName);
}

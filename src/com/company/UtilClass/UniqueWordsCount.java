package com.company.UtilClass;

import com.company.Interface.ReadFileIntf;
import com.company.Interface.UniqueWordsCountIntf;
import com.company.ReadFile;

import java.util.HashSet;
import java.util.Set;

public class UniqueWordsCount implements UniqueWordsCountIntf {

    private ReadFileIntf readFileObj = new ReadFile();
    private Set<String> textSet = new HashSet<>();

    @Override
    public int uniqueWordsCount(String fileName) {

        for (String str : readFileObj.readFile(fileName)) {

            textSet.add(str);

        }

        return textSet.size();
    }


}

package com.company.Interface;


public interface UniqueWordsCountIntf {
    int uniqueWordsCount(String fileName);
}

package com.company;

import java.util.Comparator;

public class CompareWord implements Comparator<String> {

    //Сортировка по длине
    @Override
    public int compare(String o1, String o2) {
        return o1.length() - o2.length();
    }
}

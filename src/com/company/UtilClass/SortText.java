package com.company.UtilClass;

import com.company.CompareWord;
import com.company.Interface.ReadFileIntf;
import com.company.Interface.SortTextIntf;
import com.company.ReadFile;

import java.util.List;

public class SortText implements SortTextIntf {

    private ReadFileIntf readFileObj = new ReadFile();

    @Override
    public List<String> sortText(String fileName) {

        List<String> sortList = readFileObj.readFile(fileName);
        sortList.sort(new CompareWord());
        return sortList;
    }
}

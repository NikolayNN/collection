package com.company;

import com.company.Interface.ReadFileIntf;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ReadFile implements ReadFileIntf {

    @Override
    public List<String> readFile(String fileName) {
        List<String> textList = null;

        try (FileReader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {

            try {
                StringBuilder text = new StringBuilder();

                String stringLine = bufferedReader.readLine();

                while (stringLine != null) {

                    text.append(stringLine);
                    text.append(" ");
                    stringLine = bufferedReader.readLine();

                }

                String stringText = text.toString().replaceAll("\\p{Punct}", "");
                textList = Arrays.asList(stringText.split(" "));

            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
            }


        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());

        }

        return textList;
    }
}
